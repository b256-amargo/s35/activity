
// JS Server

const express = require("express");
// allows us to access the methods and functions that will help us create a server
const mongoose = require("mongoose");
// allows us creation of schemas to model our data structures, also has access to different methods for manipulating our database
const port = 3000;
const app = express();

// -----------------------------

// MongoDB (Cloud Database) Connection

mongoose.connect("mongodb+srv://admin:admin1234@b256amargo.fojeanf.mongodb.net/B256_to-do?retryWrites=true&w=majority", {
// admin1234 and B256_to-do manually inserted
	useNewUrlParser: true,
	useunifiedTopology: true
	// allows us to avoid any current and future errors while connecting to MongoDB
});

// -----------------------------

// Middlewares

app.use(express.json());
app.use(express.urlencoded({extended: true}));


// -------------------------------------------------------------

// Checking the connection

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error"));
db.once("open", () => console.log(`Connection to the cloud database successful`));

// -----------------------------

// Mongoose Schema
	// Schemas determine the structure of the document to be written in the database
	// A template/blueprint in a nutshell

const taskSchema = new mongoose.Schema({
// Use the Schema() constructor of the Mongoose module to create a new Schema object
// The "new" keyword creates a new Schema
	name: String,
	// Define the fields with the corresponding data type
	// For a task, it needs a "task name" and "task status"
	// There is a field called "name" and its data type is "String"
	status: {
	// There is a field called "status" that is a "String" and the default value is "pending"
		type: String,
		default: "pending"
		// Default values are the predefined values for a field if we don't put any value
	}
});

// -----------------------------

// Models
	// Uses schemas and are used to create/instantiate objects that correspond to the schema
	// Models use Schemas and they act as the middleman from the server (JS code) to our database
	// request > server > schema > database

const Task = mongoose.model("Task", taskSchema);
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection
// Using Mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection in postman


// -------------------------------------------------------------

// Creating a New Task
/*
	Business Logic:
		1. Add a functionality to check if there are duplicate tasks
			- If the task already exists in the database, we return an error
			- If the task doesn't exist in the database, we add it in the database
		2. The task data will be coming from the request's body
		3. Create a new Task object with a "name" field/property
		4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/task", (request, response) => {
// "request" and "response" and all names in orange can be any name
	Task.findOne({name: request.body.name}).then((result, error) => {
	// mongoose's findOne() method is similar to mongoDB's find() method
	// findOne() returns the first document that matches the search criteria as a single object
	// findOne() can send the possible result or error in another method called then() for further processing.
		if(result !== null && result.name == request.body.name){
			return response.send(`Duplicate Task Found`)
			// return needed here? since this is still inside the line86 function, which is inside the main line84 function
		}
		else{
			let newTask = new Task({
				name: request.body.name
			});
			// task creation

			newTask.save().then((savedTask, savedErr) => {
			// save() for saving the created task into the database
				if(savedErr){
					return console.error(savedErr);
				}
				else{
					return response.status(201).send("New Task Created");
					// 201 means successfully created
				};
			});
		};
	});
});

// -----------------------------

// Getting All Tasks
/*
	Business Logic:
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks", (request, response) => {
	Task.find({}).then((result, error) => {
	// find({}) means find all with {} on the database
		if(error){
			return console.error(error)
		}
		else{
			response.status(200).json({
				data: result
				// will show { data: [{ documents found by find() }] }
			})
		};
	});
});

// -------------------------------------------------------------

// ACTIVITY

// (1) Schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

// (2) Model
const User = mongoose.model("User", userSchema);

// (3) Signup Method
app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username}).then((result, error) => {
		if(result !== null && result.username == request.body.username){
			return response.send(`Duplicate user found`);
		}
		else{
			let newUser = new User({
				username: request.body.username,
				password: request.body.password
			});

			newUser.save().then((savedUser, savedErr) => {
				if(savedErr){
					return console.error(savedErr);
				}
				else{
					return response.status(201).send("New user registered");
				};
			});
		};
	});
});


// -------------------------------------------------------------

app.listen(port, () => console.log(`Server is now currently running at port ${port}`));
